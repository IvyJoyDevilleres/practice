import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { infos } from './http-sample/model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // retrieved: infos[];

  private _url = "https://jsonplaceholder.typicode.com/users"

  constructor(
    private _http: HttpClient
  ) { }

  getData() {
    return  this._http.get<infos>(this._url)
    
  }
}
