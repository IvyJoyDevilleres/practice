import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { infos } from '../http-sample/model';

@Component({
  selector: 'app-http-sample',
  templateUrl: './http-sample.component.html',
  styleUrls: ['./http-sample.component.css']
})
export class HttpSampleComponent implements OnInit {

  public retrieved: infos;

  columns=["id","name","email","company"]

  constructor(private appService: ApiService ) { }

  ngOnInit() {
   this.appService.getData().subscribe((datas: infos)=>{
     this.retrieved= datas;
     console.log(this.retrieved)
   });
   
  }

}
