import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent implements OnInit {

  public info: any;

  public fname:string;

  public email:string;
  
  public genderValue:string;

  //....... submitted variables

  public name: string;

  public EmailAdd: string;

  public Gender: string;

  condition=false;

  constructor() { }

  ngOnInit() {
  }

  onSubmit(){
    this.info={"name":this.fname,"email":this.email,"Gender":this.genderValue};
    console.log(this.info);
    this.name=this.info.name;
    this.EmailAdd=this.info.email;
    this.Gender=this.info.Gender;
    this.condition=true;
  }
  
  Edit(){
    this.condition=false;
  }

}
