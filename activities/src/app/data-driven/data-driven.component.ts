import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-data-driven',
  templateUrl: './data-driven.component.html',
  styleUrls: ['./data-driven.component.css']
})
export class DataDrivenComponent implements OnInit {

  public Zip:number;

  public mail:string;

  public First:String;

  public last:String;

  public strt:String;

  public ct:String;

  public stit:String;

  public gson: any;


  reactiveform= this.fb.group({
    firstname:['',Validators.required],
    lastname:['',Validators.required],
    email: ['',Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    Street:['',Validators.required],
    City:['',Validators.required],
    State:['',Validators.required],
    Code:['',Validators.required],
  })

  
  
  constructor( private fb: FormBuilder) { }

  ngOnInit() {
  }

  submit(){
    this.gson=this.reactiveform.value;
  }

}
