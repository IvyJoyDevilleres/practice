import { Component, OnInit, Output, Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @Input() fname: string;
  @Input() Nrld: boolean;
  @Input() gndr: string;

  yes="Application was APPROVED!";

  no="Application was REJECTED!";

  @Output() Cevent = new EventEmitter();
  @Output() dclndEvent = new EventEmitter();
  
  public Cdata: string;

  list: any[]=[];

  rejected: any[]=[];

  constructor() { }

  ngOnInit() {
  }

  onChange(value: string){
    // console.log(value);
    this.Cevent.emit(value);
    
    
  }

  approved(){
    this.list.push({"name":this.fname,"status":this.Nrld,"gender":this.gndr});
    console.log(this.list);
  }

  declined(){
    this.dclndEvent.emit({"name":this.fname,"status":this.Nrld,"gender":this.gndr});

  }

  
  

}
